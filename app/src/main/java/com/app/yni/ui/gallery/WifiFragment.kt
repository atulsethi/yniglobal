package com.app.yni.ui.gallery

import android.content.BroadcastReceiver
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.app.yni.R
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.content.Context
import android.widget.Toast
import android.util.Log

import android.content.Intent
import android.net.NetworkInfo
import android.net.wifi.WifiInfo
import android.widget.Button
import android.widget.EditText
import org.jetbrains.anko.support.v4.toast




class WifiFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        val usernameEd: EditText = root.findViewById(R.id.username)
        val passwordEd: EditText = root.findViewById(R.id.password)
        val connectButton: Button = root.findViewById(R.id.connect)


// need to validate all fields

        connectButton.setOnClickListener{


            connectToWiFi(usernameEd.text.toString(),passwordEd.text.toString())


        }



        return root
    }











    fun connectToWiFi(ssid:String,pass:String){
        if(isConnectedTo(ssid)){
            toast("Connected to    "+ssid)

            return
        }

        var wifiConfig=getWiFiConfig(ssid)
        if(wifiConfig==null){
            createWPAProfile(ssid,pass)
        }

        enableNework(ssid,context!!)


    }
    fun isConnectedTo(ssid: String):Boolean{
        val wm:WifiManager= context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if(wm.connectionInfo.ssid.replace("\"","") == ssid){
            return true
        }
        return false
    }
    fun getWiFiConfig(ssid: String): WifiConfiguration? {
        val wm:WifiManager= context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiList=wm.configuredNetworks
        for (item in wifiList){
            if(item.SSID != null && item.SSID.replace("\"","").equals(ssid)){
                return item
            }
        }
        return null
    }


    fun createWPAProfile(ssid: String,pass: String){
        val conf = WifiConfiguration()
        conf.SSID = ssid
        conf.preSharedKey = pass
        val wm:WifiManager= context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wm.addNetwork(conf)
    }



    fun enableNework(ssid: String, cxt: Context): Boolean {
        var state = false
        val wm = cxt.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if (wm.setWifiEnabled(true)) {
            val networks = wm.configuredNetworks
            val iterator = networks.iterator()
            while (iterator.hasNext()) {
                val wifiConfig = iterator.next()
                if (wifiConfig.SSID.replace("\"","") == ssid) {
                    state = wm.enableNetwork(wifiConfig.networkId, true)
                }

                else
                    wm.enableNetwork(wifiConfig.networkId,false)
            }
            wm.reconnect()
        }
        return state
    }


}


